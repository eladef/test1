import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'notFound',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  toItems(){
    this.router.navigate(['/items'])
  }

  constructor(private router:Router) { }

  ngOnInit() {
  }

}
