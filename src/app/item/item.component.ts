import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Router} from "@angular/router";
import { ItemsService } from 'src/app/items.service';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  
  @Input() data:any;
  @Output() myButtonClicked= new EventEmitter<any>();

  text;
  $key;
  id;
  name;
  price;
  stock;
  status:boolean;
  showtheButton = true; 
  tempText;
  showEditField=false;


  checkChange()
{

  this.itemsService.updateStatus(this.$key,this.status);
}

  showButton(){
    if(!this.showEditField){
    this.showtheButton=true;
  }
    }
  hideButton(){
  this.showtheButton=false;
  }
  delete(){
    this.itemsService.delete(this.$key);
  }

  showEdit()
{
  this.tempText = this.text;
  this.showEditField = true; 
  this.showtheButton = false;
}

  constructor(private router:Router, private itemsService:ItemsService) {
  }

  ngOnInit() {
    this.$key = this.data.$key; 
    this.name = this.data.name;
    this.price = this.data.price;
    this.stock = this.data.stock;
    this.status = this.data.status;
  }

  deleteRow() {
    this.showtheButton = false;
    this.myButtonClicked.emit(this);
  }

  cancel()
{
  this.text = this.tempText;
  this.showEditField = false;
}


}
