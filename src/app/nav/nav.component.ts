import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { AuthService } from 'src/app/auth.service';


@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  toItems(){
    this.router.navigate(['/items'])
  }
  toLogin(){
    this.router.navigate(['/login'])
  }
  toLogout()
  {
    this.authService.logout().
    then(value =>{
    this.router.navigate(['/login'])
    }).catch(err=> {
      console.log(err)
    })
  } 

  constructor(private router:Router , private authService:AuthService) { }

  ngOnInit() {
  }

}
