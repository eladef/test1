import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; // פקודה להבאת הנתיבים

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { ItemsComponent } from './items/items.component';
import { ItemComponent } from './item/item.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';

//app modules:
import {MatCardModule} from '@angular/material/card';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
//for firebase:
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

//for 2 way bunding:
import{FormsModule} from '@angular/forms';

//for eviromests:
import {environment} from '../environments/environment';
import { GreetingsComponent } from './greetings/greetings.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ItemsComponent,
    ItemComponent,
    LoginComponent,
    NotFoundComponent,
    GreetingsComponent
  ],
  imports: [
    BrowserModule,
    MatCardModule,
    MatListModule,
    MatTableModule,
    MatButtonModule,
    MatCheckboxModule,
    AngularFireModule.initializeApp (environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    BrowserAnimationsModule,
    MatInputModule,
    FormsModule,
    RouterModule.forRoot([
      {path:'', component:ItemsComponent},
      {path:'items', component:ItemsComponent},
      {path:'item', component:ItemComponent},
      {path:'login', component:LoginComponent},
      {path:'greetings', component:GreetingsComponent},
      {path:'**', component:NotFoundComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
