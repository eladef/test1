import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth'
import {Observable} from 'rxjs';
import {AngularFireDatabase} from '@angular/fire/database';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {


  addList(text:string,status:boolean){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/items').push({'text':text, 'status':false});
    })
  }

  delete(key){
    this.authService.user.subscribe(user=>{
     this.db.list('users/'+user.uid+'/items').remove(key);
    })
  }

  updateStatus(key:string, status:boolean)
  {
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/items').update(key,{'status':status});
    })
    
  }


  constructor(private db:AngularFireDatabase, private authService:AuthService) { }
}
