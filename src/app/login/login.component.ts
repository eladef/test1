import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from "@angular/router";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


email: string;
password: string;
error='';

login(){
  this.authService.login(this.email , this.password).then(value=>{
      this.router.navigate(['/items']);
      }).catch(err =>{
        this.error = err;
        console.log(this.email+" "+this.password);
        console.log(err);
})
}


  constructor(private authService:AuthService , private router:Router) { }

  ngOnInit() {
  }

}
